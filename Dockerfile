FROM python:rc-alpine

RUN pip install td-watson
RUN pip uninstall -y click && pip install click==7.1.2 # Hotfix
RUN apk add --no-cache bash fzf less nano coreutils sed

COPY ws.sh /

ENV SHELL=/bin/bash
CMD ["/ws.sh"]
