#!/bin/bash

# INFO:
# Default Watson config folder it's in: ~/.config/watson/

command -v fzf    &> /dev/null || exit 1
command -v watson &> /dev/null || exit 1
command -v sed    &> /dev/null || exit 1
command -v grep   &> /dev/null || exit 1
command -v tail   &> /dev/null || exit 1
command -v head   &> /dev/null || exit 1

export FZF_DEFAULT_OPTS="--cycle --tac --ansi --bind ctrl-a:select-all --preview-window=80%"
unset FTO #FZF_TEMP_OPTS

# Prompt a question with default option.
# Eg: questionYN "default it's NO"  n
# Eg: questionYN "default it's YES"
questionYN(){
    local opt
    local SN

    if [[ "${2,,}" == "n" ]]; then
        SN=n
        SNP="(y/N)"
    else
        SN=y
        SNP="(Y/n)"
    fi
    while [[ "$opt" != "y" && "$opt" != "n" ]]; do
        read -rn1 -p "$1 ${SNP}: " opt
        if [[ "$opt" == "" ]]; then
            opt="$SN"
        else
            echo
        fi
        opt="${opt,,}"
    done
    [[ $opt == y ]]
}

# Aux funct for start().
countup(){
    local opt
    local start=$(date +%s)
    local pause
    local paused=0
    while :; do
        echo -ne "$(date -u --date @$(($(date +%s) - $start)) +%T)       \r"
        read -st1 -n1 opt
        [[ $opt =~ [pP] ]] && {
            echo -ne "-- PAUSED --\r"
            pause=$(date +%s)
            watson stop &> /dev/null
            read -sn1 opt
            pause=$(($(date +%s) - $pause))
            paused=$(($paused + $pause))
            start=$(($start + $pause))
            echo -e "\e[1;32m$(date -u --date @$(($(date +%s) - $start)) +%T)\e[0;0m        "
            echo -e "\e[31m$(date -u --date @$pause +'%T')\e[0m \e[1;31m$(date -u --date @$paused +'%T')\e[0;0m"
            [[ ! $opt =~ [qQ] ]] && {
                watson restart &> /dev/null || opt=q
            }
        }
        [[ $opt =~ [qQ] ]] && {
            watson stop &> /dev/null
            echo
            echo -e "\e[36mTotal:\e[0m"
            echo -e "\t\e[1;32mRunning:\e[0;0m $(date -u --date @$(($(date +%s) - $start)) +%T)"
            echo -e "\t\e[1;31mStopped:\e[0;0m $(date -u --date @$paused +'%T')"
            break
        }
    done
}

_list(){
    local FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS $FTO"
    local i
    for i in "$@"; do
        echo "$i"
    done | fzf
}

aggregate(){
    local opt param i cmd
    opt=$(FTO=-m _list --project --tag ALL)
    echo "$opt" | grep -q ALL
    [[ $? -ne 0 && -n $opt ]] && {
        while read param; do
            case $param in
                --project)
                    while read i; do
                        cmd+=" --project \"$i\""
                    done < <(FTO=-m _list "$(watson projects)")
                ;;
                --tag)
                    while read i; do
                        cmd+=" --tag \"$i\""
                    done < <(FTO=-m _list "$(watson tags)")
                ;;
            esac
        done <<< $opt
    }
    eval watson aggregate $cmd || exit $?
}

config(){
    ! echo "ToDo: config"
}

edit(){
    local log date frame preview
    log="$(watson --color log --all --no-reverse --no-pager)"
    preview="'day={};sed -n \"/\$day/,\\\${/^\$/q;s/\\t//;p}\" <<<\$log'"
    date="$(
        log=$log \
        FTO="--preview=$preview" \
        _list "$(sed -n '/^[^\t]/{s/ (.*$//p}' <<<$log)"
    )"
    [[ -z $date ]] && return
    frame="$(_list "$(sed -n "0,/$date/d;/^\$/q;s/\t//p" <<<$log)" | sed 's/ .*//')"
    [[ -z $frame ]] && return
    watson edit "$frame"
    read -sn1
}

log(){
    local cmd log preview date i

    # Projects
    while read i; do
        cmd+=" -p \"$i\""
    done < <(FTO="-m --preview='watson --color log -Gp {}'" _list "$(watson projects)")

    # Tags
    while read i; do
        cmd+=" -T \"$i\""
    done < <(FTO="-m --preview='watson --color log -GT {} $cmd'" _list "$(watson tags)")

    log="$(eval watson --color log --all --no-reverse --no-pager $cmd)"

    preview='date=$(date -d {} +%F);'
    preview+='watson --color log -f $date -t $date'
    date="$(
        log=$log \
        FTO="--preview='$preview'" \
        _list "$(sed -n '/^[^\t]/{s/ (.*$//p}' <<<$log)"
    )"
    if [[ -n $date ]]; then
        cmd+=" -f $(date -d "$date" +%F)"
    else
        cmd+=" --all"
    fi

    eval watson log  \
        --no-reverse \
        --pager      \
        $cmd         ||
        exit $?
}

merge(){
    ! echo "ToDo: merge"
}

remove(){
    local items frame log log_frame day

    items=$(FTO=-m _list "$(watson log --all -Gs | tail -n+2)")
    [[ -z $items ]] && return
    log=$(watson --color log --all --no-pager --no-reverse)

    echo Removing:
    while read frame; do
        frame=${frame%%,*}
        log_frame=$(sed -n "/^[^\t]/h;/$frame/{x;p;g;p;q}" <<<$log)

        [[ $day != $(sed -n 1p <<<$log_frame) ]] && {
            day=$(head -n1 <<<$log_frame)
            echo "${day% (*}"
        }
        sed -n 2p <<<$log_frame
        watson remove $frame --force >/dev/null
    done <<<$items
}

rename(){
    ! echo "ToDo: rename"
}

report(){
    local log preview date watson_date i

    log="$(eval watson --color log --all --no-reverse --no-pager)"

    # Dates
    preview='date=$(date -d {} +%F);'
    preview+='watson --color report -f $date -t $date | tail -n+3'
    date="$(
        log=$log \
        FTO="-m --preview='$preview'" \
        _list "$(sed -n '/^[^\t]/{s/ (.*$//p}' <<<$log)"
    )"
    [[ -n $date ]] || return 0

    {
        # For each selected day.
        while read i; do
            watson_date=$(date -d "$i" +%F)
            eval watson         \
                --color         \
                log             \
                --no-reverse    \
                --no-pager      \
                -f $watson_date \
                -t $watson_date ||
                exit $?
            echo
            eval watson         \
                --color         \
                report          \
                --no-pager      \
                -f $watson_date \
                -t $watson_date |
              tail -n+3         ||
                exit $?
            echo
            echo
        done <<<$date
    } | less -rS
}

checkStatus(){
    local current
    current=$(LC_ALL=C watson status -p)
    [[ $current == "No project started." ]] || {
        echo "There is a current project running:"
        watson status
        questionYN "Do you want to stop it?" && {
            watson stop
        }
    }
}

restart(){
    local frame pro tag from to

    checkStatus || return 0

    frame=$(_list "$(watson log --all -Gs | tail -n+2)")
    [[ -z $frame ]] && return

    watson restart -s ${frame%%,*}
    countup

    pro=${frame#*,}
    from=${pro%%,*}
    pro=${pro#*,}
    to=${pro%%,*}
    pro=${pro#*,}
    tag=${pro#*,}
    pro=${pro%%,*}
    [[ -n $tag ]] && {
        tag=${tag#\"}
        tag=${tag%\"}
        tag=${tag//\"/\\\"}
        tag=${tag//\'/\\\'}
        tag=${tag//\`/\\\`}
        tag=-T\"${tag//, /\" -T\"}\"
    }

    eval watson log        \
        --all              \
        --no-reverse       \
        --no-pager         \
        -f \"$from\"       \
        -t \"$to\"         \
        --project \"$pro\" \
        "$tag"             ||
        exit $?
    read -sn1
}

start(){
    local dt it t=1 cmd from to lsttags lstcmd preview
    local p

    checkStatus || return 0

    p=$(_list "$(watson projects; echo -e '\e[36mNEW\e[0m')")

    [[ $p == NEW ]] && read -ep"Project name: " p
    p=${p//,/_}
    [[ -z $p ]] && return

    from="--from \"$(date +%F\ %T)\""
    cmd="'$p'"
    lsttags=$(
        watson tags
    )
    lstcmd=$(
        echo -e '\e[31mDELETE\e[0m'
        echo -e '\e[36mNEW\e[0m'
        echo -e '\e[32mSTART\e[0m'
    )
    while [[ -n $t ]]; do
        t=$(
            preview=${preview//\\/\\\\}
            preview=${preview//\"/\\\"}
            preview=${preview//\$/\\\$}
            preview=${preview//\`/\\\`}

            preview=${preview//\\/\\\\}
            preview=${preview//\"/\\\"}
            FTO="--preview=\"echo -e \\\"\\\e[32m$p:\\\e[0m\\\n$preview\\\"\""
            FTO+=" --color='preview-fg:75'"
            _list "$lsttags"$'\n'"$lstcmd"
        )
        [[ -z        $t ]] && return
        [[ START  == $t ]] && break
        [[ DELETE == $t ]] && {
            [[ -z $preview ]] && continue
            dt=$(_list "$preview")
            it=$dt
            dt=${dt//\\/\\\\}
            dt=${dt//\$/\\\$}
            [[ -n $dt ]] && grep -E "^$dt\$" <<<$preview && {
                preview=$(sed "/^$dt$/d" <<<$preview)
                lsttags+=$'\n'$it
            }
            continue
        }
        [[ NEW    == $t ]] && {
            read -erp"Tag name: " t
            [[ -z $t ]] && continue
            t=${t//,/_}
        }
        preview+="${preview:+$'\n'}$t"
        t=${t//\\/\\\\}
        t=${t//\$/\\\$}
        lsttags=$(sed "/^$t$/d" <<<$lsttags)
        t=${t//\"/\\\"}
        t=${t//\`/\\\`}
        cmd+=" +\"$t\""
    done

    eval watson start $cmd || exit $?
    countup
    to="--to \"$(date +%F\ %T)\""

    eval watson log                  \
        --all                        \
        --no-reverse                 \
        --no-pager                   \
        $from $to                    \
        --project ${cmd// +\"/ -T\"} ||
        exit $?
    read -sn1
}

stop(){
    watson stop
}

sync(){
    ! echo "ToDo: sync"
}

OPTIONS=(
    aggregate
    config
    edit
    log
    merge
    remove
    rename
    report
    restart
    start
    stop
    sync
    EXIT
)

_fzf_menu(){
    case $1 in
        aggregate)
            echo "ToDo";;
        config)
            echo "ToDo";;
        edit)
            echo "Edit";;
        merge)
            echo "ToDo";;
        rename)
            echo "ToDo";;
        report|start)
            watson --color report;;
        stop)
            watson --color status;;
        sync)
            watson config backend.url;;
        restart|remove|log)
            watson --color log -R;;
        *)
            echo -e "\e[1;36mPROJECTS:\e[0;0m"
            watson --color projects
            echo
            echo -e "\e[1;36mTAGS:\e[0;0m"
            watson --color tags
        ;;
    esac
}
export -f _fzf_menu

menu(){
    local opt
    opt=$(FTO="--preview='_fzf_menu {}'" _list "${OPTIONS[@]}")
    case $opt in
        aggregate)
            aggregate;;
        config)
            config   ;;
        edit)
            edit     ;;
        log)
            log      ;;
        merge)
            merge    ;;
        remove)
            remove   ;;
        rename)
            rename   ;;
        report)
            report   ;;
        restart)
            restart  ;;
        start)
            start    ;;
        stop)
            stop     ;;
        sync)
            sync     ;;
        EXIT|'')
            exit 0   ;;
        *)
            return 1
    esac
}

while [[ $? -eq 0 ]]; do
    menu
done
