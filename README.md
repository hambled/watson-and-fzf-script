# Watson & fzf wrapper

Script para poder manejar la herramienta de [Watson][Watson github] de forma cómoda usando como interfaz [fzf][fzf github].


## Uso como contenedor
### Crear el contenedor
```bash
docker build -t ws .
```

### Arrancar
Es recomendable crear un volumen para no perder los datos.
En este caso el volumen es `ws-tasts`.

```bash
docker run                                     \
    --rm                                       \
    --tty                                      \
    --interactive                              \
    --name=ws                                  \
    -e TZ=$(curl -s https://ipapi.co/timezone) \
    -e EDITOR=nano                             \
    --volume ws-tasks:/root/.config/watson     \
    ws
```

## ToDo
Siguen quedando varias cosas por implementar, pero el script ya es usable.
- [ ] Traducir al inglés.
- [ ] Terminar de implementar los comandos que faltan.
- [ ] Mejorar la documentación.
- [ ] Crear un asciinema.

[Watson github]: https://github.com/TailorDev/Watson "Repositorio oficial de Watson"
[fzf github]:    https://github.com/junegunn/fzf     "Repositorio oficial de fzf"
